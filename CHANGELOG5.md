// Snake.cpp : Definiert den Einstiegspunkt für die Konsolenanwendung.
//

#include "stdafx.h"
#include <time.h>
#include <conio.h>


const int g = 18;


int main()
{

	int headx, heady;
	int snakex[400], snakey[400];
	char move;
	char lastmove, lastmoveend;
	int punktx = 0, punkty = 0;
	int j, i, l, lschleife;
	int punktestand;
	int sleeptime;
	bool gameover = false;
	char w;

	srand(time(NULL));

	formen("s");
	groesse(g, g);

	do{
		headx = 3;
		heady = 3;
		l = 3;
		sleeptime = 300;
		punktestand = 0;
	//Anfangskörper
	for (i = 0; i <= l; i++) {
		snakex[i] = headx;
		snakey[i] = heady;
	}


	//Feld färben
	farben(LIGHTGREEN);

	//Punkt
	punktx = rand() % g;
	punkty = rand() % g;
	farbe2(punktx, punkty, RED);

	//Startpunkt
	farbe2(3, 3, BLUE);


	move = 'd';

	printf("Punkte: %d", punktestand);

	do {		//Spieldurchlauf

		do {		//Tastatureingabe


			switch (move) {
			case 'w':
				headx += 0;
				heady += 1;
				break;
			case 'a':
				headx += (-1);
				heady += 0;
				break;
			case 'd':
				headx += 1;
				heady += 0;
				break;
			case 's':
				headx += 0;
				heady += (-1);
				break;
			}
			lastmove = move;


			//Schlange körperx
			for (j = 1, lschleife = l; lschleife >= j; lschleife--) {
				snakex[lschleife] = snakex[lschleife - 1];
			}
			//Schlange körpery
			for (j = 1, lschleife = l; lschleife >= j; lschleife--) {
				snakey[lschleife] = snakey[lschleife - 1];
			}

			//Kopf in Schlange
			snakex[0] = headx;
			snakey[0] = heady;

			//Aus der Wand
			if (heady >= g){
				gameover = true;
				break;
			}
			else if (heady < 0) {
				gameover = true;
				break;
			}
			else if (headx >= g) {
				gameover = true;
				break;
			}
			else if (headx < 0) {
				
				gameover = true;
				break;
			}
			//Schlange färben
			farbe2(headx, heady, BLUE);

			//Schwanz streichen
			farbe2(snakex[l], snakey[l], LIGHTGREEN);

			Sleep(sleeptime);

			//Punkt
			//
			if (headx == punktx && heady == punkty) {
				punktx = rand() % g;
				punkty = rand() % g;
				l += 1;

				
					for (i = 0; i <= l; i++) {
						if (punktx == snakex[i] && punkty == snakey[i]) {
							punktx = rand() % g;
							punkty = rand() % g;
						}
				}
					

				snakex[l] = snakex[l - 1];
				snakey[l] = snakey[l - 1];
				farbe2(punktx, punkty, RED);
				punktestand += 100;
				if (punktestand % 100 == 0 && sleeptime >= 60) {

					sleeptime -= 10;
				}

				printf("\rPunkte: %d", punktestand);
			}
			//
			farbe2(punktx, punkty, RED);

			//Game over
			for (i = 1; i <= l; i++) {
				if (headx == snakex[i] && heady == snakey[i]) {
					gameover = true;
					farbe2(headx, heady, DARKRED);
				}
			}
			if (gameover == true) {
				break;
			}


		} while (!_kbhit());

		//Entgegengesetzte Richtung verhindern
		move = _getch();
		if (lastmove == 'w'&& move == 's') {
			move = lastmove;
		}
		else if (lastmove == 's'&& move == 'w') {
			move = lastmove;
		}
		if (lastmove == 'a'&& move == 'd') {
			move = lastmove;
		}
		else if (lastmove == 'd'&& move == 'a') {
			move = lastmove;
		}

		//gameover
		if (gameover == true) {
			break;
		}

	} while (move != 32);
	printf("\nGAME OVER");
	printf("\nWollen sie weiter spielen ? (j/n)\n");
	w = _getch();
	gameover = false;
}while (w == 'j');

	return 0;
}

