// Snake.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung. //

#include "stdafx.h"
#include <conio.h> 
#include <time.h>

const int g = 18;

struct head {
	int x;
	int y;
};

struct punkt {
	int x;
	int y;
};

struct snakebody {
	int x[400];
	int y[400];
};

int main() {
	char move;
	char lastmove, lastmoveend;

	int j, i, l, lschleife;
	int punktestand;
	int sleeptime;
	bool gameover = false;
	char w;

	struct head h;
	struct punkt p;
	struct snakebody sb;

	srand(time(NULL));

	formen("s");
	groesse(g, g);

	do {
		h.x = 3;
		h.y = 3;
		l = 3;
		sleeptime = 300;
		punktestand = 0;
		//Anfangsk�rper
		for (i = 0; i <= l; i++) {
			sb.x[i] = h.x;
			sb.y[i] = h.y;
		}


		//Feld f�rben
		farben(BLACK);

		//Punkt
		p.x = rand() % g;
		p.y = rand() % g;
		farbe2(p.x, p.y, NAVY);

		//Startpunkt
		farbe2(3, 3, WHITE);


		move = 'd';

		printf("Punkte: %d", punktestand);

		do {        //Spieldurchlauf

			do {        //Tastatureingabe


				switch (move) {
				case 'w':
					h.x += 0;
					h.y += 1;
					break;
				case 'a':
					h.x += (-1);
					h.y += 0;
					break;
				case 'd':
					h.x += 1;
					h.y += 0;
					break;
				case 's':
					h.x += 0;
					h.y += (-1);
					break;
				}
				lastmove = move;


				//Schlange k�rperx
				for (j = 1, lschleife = l; lschleife >= j; lschleife--) {
					sb.x[lschleife] = sb.x[lschleife - 1];
				}
				//Schlange k�rpery
				for (j = 1, lschleife = l; lschleife >= j; lschleife--) {
					sb.y[lschleife] = sb.y[lschleife - 1];
				}

				//Kopf in Schlange
				sb.x[0] = h.x;
				sb.y[0] = h.y;

				//Aus der Wand
				if (h.y >= g) {
					farbe2(sb.x[1], sb.y[1], DARKRED);
					gameover = true;

					break;
				}
				else if (h.y < 0) {
					farbe2(sb.x[1], sb.y[1], DARKRED);
					gameover = true;
					break;
				}
				else if (h.x >= g) {
					farbe2(sb.x[1], sb.y[1], DARKRED);
					gameover = true;
					break;
				}
				else if (h.x < 0) {
					farbe2(sb.x[1], sb.y[1], DARKRED);
					gameover = true;
					break;
				}
				//Schlange f�rben
				farbe2(h.x, h.y, WHITE);

				//Schwanz streichen
				farbe2(sb.x[l], sb.y[l], BLACK);

				Sleep(sleeptime);

				//Punkt
				//
				if (h.x == p.x && h.y == p.y) {
					p.x = rand() % g;
					p.y = rand() % g;
					l += 1;


					for (i = 0; i <= l; i++) {
						if (p.x == sb.x[i] && p.y == sb.y[i]) {
							p.x = rand() % g;
							p.y = rand() % g;
						}
					}


					sb.x[l] = sb.x[l - 1];
					sb.y[l] = sb.y[l - 1];
					farbe2(p.x, p.y, NAVY);
					punktestand += 100;
					if (punktestand % 100 == 0 && sleeptime >= 60) {

						sleeptime -= 10;
					}

					printf("\rPunkte: %d", punktestand);
				}
				//
				farbe2(p.x, p.y, NAVY);

				//Game over
				for (i = 1; i <= l; i++) {
					if (h.x == sb.x[i] && h.y == sb.y[i]) {
						gameover = true;
						farbe2(h.x, h.y, DARKRED);
					}
				}
				if (gameover == true) {
					break;
				}


			} while (!_kbhit());

			//Entgegengesetzte Richtung verhindern
			move = _getch();
			if (lastmove == 'w'&& move == 's') {
				move = lastmove;
			}
			else if (lastmove == 's'&& move == 'w') {
				move = lastmove;
			}
			if (lastmove == 'a'&& move == 'd') {
				move = lastmove;
			}
			else if (lastmove == 'd'&& move == 'a') {
				move = lastmove;
			}

			//gameover
			if (gameover == true) {
				break;
			}

		} while (move != 32);
		printf("\nGAME OVER");
		printf("\nWollen sie weiter spielen ? (j/n)\n");
		w = _getch();
		gameover = false;

	} while (w == 'j');

	return 0;

}
