-   Wir haben uns für das Thema Snake entschieden. 

-   Die "Schlange" bewegt sich nach belieben des Spielers entweder Horizontal oder Vertikal.
    Das Ziel ist es, dass die "Schlange" einen Punkt einsammelt der zufällig auf dem Feld ist. Sobald ein Punkt eingesammelt wurde, erscheint ein weiterer auf einer anderen zufälligen Stelle.
    Sobald die "Schlange" einen Punkt aufgesammelt hat, wird diese um ein Feld größer.
    
-   Die vermutlich größte Herausforderung wird sein, dass die "Schlange nicht gegen ihren eigenen Körper sowie auch gegen den Rand des Feldes stoßen darf. Falls das geschieht, hat der Spieler das Spiel verloren.

    Den Lösungsansatz stellen wir uns folgender maßen vor: Die "Schlange" stellen wir mit einem Array dar. Mit 2 for-Schleifen werden wir überprüfen, ob das Feld doppelt belegt ist.
    Ob die "Schlange" aus dem Feld wandert, überprüfen wir mit der dynamischen größe des Feldes (x,y).