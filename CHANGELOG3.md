// Snake.cpp : Definiert den Einstiegspunkt für die Konsolenanwendung.
//

#include "stdafx.h"
#include <time.h>
#include <conio.h>


const int g = 21;


int main()
{

	int headx = 3, heady = 3;
	int snakex[21], snakey[21];
	int endx = (g / 2), endy = (g / 2);
	char move;
	int xrichtung = 0, yrichtung = 0;
	char lastmove, lastmoveend;
	int punktx = 0, punkty = 0;
	int j, i, l = 3, lschleife;
	int punktestand=0;
	int sleeptime = 400;

	srand(time(NULL));

	formen("s");
	groesse(g, g);

	//Anfangskörper
	for (i = 0; i <= l; i++) {
		snakex[i] = headx;
		snakey[i] = heady;
	}


	//Feld färben
	farben(WHITE);

	//Punkt
	punktx = rand() % 21;
	punkty = rand() % 21;
	farbe2(punktx, punkty, RED);

	//Startpunkt
	farbe2(3, 3, BLUE);


	move = 'd';

	printf("Punkte: %d", punktestand);

	do {		//Spieldurchlauf

		do {		//Tastatureingabe


			switch (move) {
			case 'w':
				xrichtung = 0;
				yrichtung = 1;
				break;
			case 'a':
				xrichtung = (-1);
				yrichtung = 0;
				break;
			case 'd':
				xrichtung = 1;
				yrichtung = 0;
				break;
			case 's':
				xrichtung = 0;
				yrichtung = (-1);
				break;
			}

			headx += xrichtung;
			heady += yrichtung;
			lastmove = move;


			//Schlange körperx
			for (j = 1, lschleife = l; lschleife >= j; lschleife--) {
				snakex[lschleife] = snakex[lschleife - 1];
			}
			//Schlange körpery
			for (j = 1, lschleife = l; lschleife >= j; lschleife--) {
				snakey[lschleife] = snakey[lschleife - 1];
			}

			//Kopf in Schlange
			snakex[0] = headx;
			snakey[0] = heady;


			//Schlange färben
			farbe2(headx, heady, BLUE);

			//Schwanz streichen
			farbe2(snakex[l], snakey[l], WHITE);

			Sleep(sleeptime);

			//Punkt
			if (headx == punktx && heady == punkty) {
				punktx = rand() % 21;
				punkty = rand() % 21;
				l += 1;
				snakex[l] = snakex[l - 1];
				snakey[l] = snakey[l - 1];
				farbe2(punktx, punkty, RED);
				punktestand += 100;
				if (punktestand % 500 == 0) {
					sleeptime -= 100;
				}

				printf("\rPunkte: %d", punktestand);
			}

			farbe2(punktx, punkty, RED);

		} while (!_kbhit());

		//Entgegengesetzte Richtung verhindern
		move = _getch();
		if (lastmove == 'w'&& move == 's') {
			move = lastmove;
		}
		else if (lastmove == 's'&& move == 'w') {
			move = lastmove;
		}
		if (lastmove == 'a'&& move == 'd') {
			move = lastmove;
		}
		else if (lastmove == 'd'&& move == 'a') {
			move = lastmove;
		}


	} while (move != 32);



	return 0;
}

